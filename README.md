# dogstar

天狼星 基于 VUE 框架的模拟Windows 桌面效果，后台管理UI。
[预览](https://ispace-code.gitee.io/dogstar-ui)

![效果图](https://images.gitee.com/uploads/images/2020/0709/201743_c8993f86_15710.png "1.png")
![效果图](https://images.gitee.com/uploads/images/2020/0709/201734_b9a680d4_15710.png "2.png")
## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
